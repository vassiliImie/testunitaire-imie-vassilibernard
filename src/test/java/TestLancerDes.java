import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.*;

public class TestLancerDes {


    String s;
    InputParser inputParser;
    InputParser inputParserFalse;
    Exception exception;


    @Before
    public void init(){
        s = "2d5";
        inputParser = new InputParser(s);
    }

    /**
     * Test si la valeur est numérique
     *
     */
    @Test
    public void TestValeurNumerique(){

        assertTrue("La valeur doit être numerique",inputParser.checkNumeric());
    }

    /**
     * Test que les faces sont compris entre 2 et 100
     */
    @Test
    public void TestValeurFaceLimitValid(){
        assertTrue("La valeur des faces doit être entre 2 et 100",inputParser.checkFace());
    }

    /**
     * Test que la valeur est un entier positif
     */
    @Test
    public void TestValeurDesLimitValid(){
        assertTrue("La valeur des dès doit être possitif",inputParser.checkDes());

    }

    /**
     * Test que les limites de la valeurs sont mauvais
     */
    @Test
    public void TestValeurDesLimitInvalid(){
        inputParserFalse = new InputParser("2222222222222222d33333333");

        try{
            inputParser.checkDes();
        }catch (Exception e){
            exception  = e;
        }

        assertNotNull("Doit retourner une exception",exception);
    }


    /**
     * Test le lancer de plusieurs des
     */
    @Test
    public void TestPlusieursDes(){
        inputParser = new InputParser("2d4 4d5");

        String [] resultat = new String[2];

        resultat = inputParser.plusieursLancer();

        assertNotNull("Doit retourner un premier resultat",resultat[0]);
        assertNotNull("Doit retourner un deuxième resultat",resultat[1]);
    }

    /**
     * Test un modificateur
     */
    @Test
    public void TestModificateur(){
        inputParser = new InputParser("2d4-4");

        assertEquals("Doit trouver un modificateur",-4,inputParser.decouperDes().getModificateur());
    }

    @Test
    public void lancerParDefaut(){
        inputParser = new InputParser();

        asserEquals("Doit retourner une lancer par defaut de 1 des",1,inputParser.decouperDes().getNombreDeLancer());
        asserEquals("Doit retourner une lancer par defaut de 6 faces des",6,inputParser.decouperDes().getNombreDeFace());
    }

    /**
     * Doit retourner une exception, mauvais template
     */
    @Test
    public void mauvaisTemplate(){
        inputParser = new InputParser("5dd6");

        try{
            inputParser.checkDes();
        }catch (Exception e){
            exception  = e;
        }

        assertNotNull("Doit retourner une exception mauvais template",exception);
    }
}
