import classe.InputParser;
import org.junit.Test;

import static junit.framework.Assert.*;

public class TestLigneCommande {


    InputParser inputParser;


    /**
     * Test qui envoie une commande non valide
     *
     * Cela doit alors retourner une exception
     */
    @Test
    public void TestCommandeNonExiste(){

        inputParser = new InputParser("2d5 -b");

        Exception exception = null;

        try{

            inputParser.verifierCommande();
        }catch (Exception e){
            exception = e;
        }

        assertNotNull("La commande doit être non reconnu",exception);
    }

    /**
     * Test afin de voir si les commandes valides passe
     */
    @Test
    public void TestCommandeExiste(){

        inputParser = new InputParser("2d5 -s");

        Exception exception = null;

        try{
            inputParser.verifierCommande();
        }catch (Exception e){
            exception  = e;
        }

        assertNull("Le commande doit être reconnu",exception);
    }

    /**
     * Test si la commande -a et -d sont incompatible
     */
    @Test
    public void TestExclusionCommandeAvantageDesavantage(){
        inputParser = new InputParser("2d5 -a -d");

        Exception exception = null;

        try{
            inputParser.verifierCommande();
        }catch (Exception e){
            exception  = e;
        }

        assertNotNull("Le commande doit être reconnu",exception);

    }

    /**
     * Test si on peut executer plusieurs commande
     */
    @Test
    public void TestPlusieursCommande(){

        inputParser = new InputParser("2d5 -s -a");

        Exception exception = null;

        try{
            inputParser.verifierCommande();
        }catch (Exception e){
            exception  = e;
        }

        assertNull("Le commande doit être reconnu",exception);

    }

    /**
     * Verifie que le resultat est verbeux quand la commande est passé
     */
    @Test
    public void TestCommandeVerbose(){
        String str = new InputParser("2d5 -v").afficherResultat();
        String str2 = new InputParser("2d5").afficherResultat();

        assertTrue("La commande v doit être verbeuse", str.length() > str2.length());
    }

    /**
     * Test afin de voir si la commande s à un comportement correcte
     *
     * et effectue bien la somme
     */
    @Test
    public void TestCommandeSum(){

        int sum = 0;

        inputParser = new InputParser("2d5 5d4 -s");

        InputParser i1 = new InputParser();
        sum = i1.lancerDes("2d5");
        sum += i1.lancerDes("5d4");

        assertEquals("La somme doit avoir été faite entre les sommes",inputParser.commandSum(),sum);
    }

    /**
     * Test si on affiche bien un résultat pour chaque lancer de des
     */
    @Test
    public void TestCommandeSeperateValue(){

        inputParser = new InputParser("2d5 5d4 -S");

        assertEquals("Doit afficher deux resultat",2,inputParser.SeperateValue().length);
    }

    /**
     * Test afin de voir le bon comportement de la commande -o
     *
     * Fais de la relance de des
     */
    @Test
    public void TestCommandeRelancerDes(){
        inputParser = new InputParser("2d6 -o");

        int lancer1 = 6;

        assertNotEqual("Verifie que un lancer au score maximal est bien relancer",6,inputParser.commandRelance(lancer1));
    }

    /**
     * Test verifie qu'il y a bien relance pour les avantages
     */
    @Test
    public void TestCommandeAvantageRelance(){
        inputParser = new InputParser("2d5 9d10 -a");

        assertNotEqual("Doit être différent",inputParser.commandAvantage("2d5"),inputParser.commandAvantage("2d5"));
    }

    /**
     * Test si la commande retourne bien la valeur max
     */
    @Test
    public void TestCommandeAvantage(){

        inputParser = new InputParser("2d5 9d10 -a");

        int lance1= 9;
        int lance2 = 5;

        assertEquals("Doit retourner la plus grande valeur",lancer1,inputParser.compareMax(lance1,lance2));
    }

    /**
     * Test verifie qu'il y a bien relance pour les desavantage
     */
    @Test
    public void TestCommandeDesavantageRelance(){
        inputParser = new InputParser("2d5 9d10 -d");

        assertNotEqual("Doit être différent",inputParser.commandDesavantage("9d10"),inputParser.commandDesavantage("9d10"));
    }

    /**
     * Test verifie que la valeur la plus petite est renvoyer
     */
    @Test
    public void TestCommandeDesavantage(){
        inputParser = new InputParser("2d5 9d10 -d");

        int lance1 = 2;
        int lance2= 9;

        assertEquals("Doit retourner la plus petite valeur",lance1,inputParser.compareMin(lance1,lancer2));
    }

}
